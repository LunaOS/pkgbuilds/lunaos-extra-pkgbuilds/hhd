pkgname=hhd
pkgver=3.11.2
pkgrel=1
pkgdesc='Handheld Daemon. A tool for managing the quirks of handheld devices.'
arch=('x86_64')
url='https://github.com/hhd-dev/hhd'
license=('MIT')
depends=('python' 'python-setuptools' 'python-evdev' 'python-rich' 'python-yaml')
makedepends=('python-'{'build','installer','setuptools','wheel'})
optdepends=('hhd-ui: Electron UI for HHD')
source=("https://pypi.python.org/packages/source/h/${pkgname}/${pkgname}-${pkgver}.tar.gz")
sha512sums=('b3df2137882da83185f9abebef3546f98893d18327a020ff4559e827755989dea0683cf219cded9a46a7e05af8a30e463af1a9f1650ef331a1dbcc5fa56a6e23')

build() {
  cd "$pkgname-$pkgver"
  python -m build --wheel --no-isolation
}

package() {
  cd "$pkgname-$pkgver"
  python -m installer --destdir="$pkgdir" dist/*.whl

  # Install minimally necessary rules for running as a system service
  mkdir -p ${pkgdir}/usr/lib/udev/rules.d/
  mkdir -p ${pkgdir}/usr/lib/udev/hwdb.d/
  mkdir -p ${pkgdir}/usr/lib/systemd/system/
  mkdir -p ${pkgdir}/usr/share/polkit-1/actions/
  install -m644 usr/lib/udev/rules.d/83-hhd.rules ${pkgdir}/usr/lib/udev/rules.d/83-hhd.rules
  install -m644 usr/lib/udev/hwdb.d/83-hhd.hwdb ${pkgdir}/usr/lib/udev/hwdb.d/83-hhd.hwdb
  install -m644 usr/lib/systemd/system/hhd@.service ${pkgdir}/usr/lib/systemd/system/hhd@.service
}
